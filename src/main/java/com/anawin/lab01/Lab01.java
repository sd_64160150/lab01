/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.anawin.lab01;

import java.util.Scanner;

/**
 *
 * @author anawi
 */
public class Lab01 {
    private char[][] board;
    private void createBoard(){
        System.out.println("-------------");
        for(int i = 0; i < 3;i++){
            System.out.print("| ");
            for (int j = 0; j < 3; j++){
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }
    private void newBoard(){
        board = new char[3][3];
        for (int i = 0 ; i < 3 ; i++){
            for (int j = 0 ; j < 3 ; j++){
                board[i][j] = '-';
            }
        }
    }
    private boolean hasWon(){
        for (int i = 0; i < 3; i++){
            if(board[i][0] != '-' && board[i][0] == board[i][1] && board[i][0] == board[i][2]){
                return true;
            }
        }
        for (int i = 0; i < 3; i++){
            if(board[0][i] != '-' && board[0][i] == board[1][i] && board[0][i] == board[2][i]){
                return true;
            }
        }
        if (board[0][0] != '-' && board[0][0] == board[1][1] && board[0][0] == board[2][2]){
            return true;
        }
        if (board[0][2] != '-' && board[0][2] == board[1][1] && board[0][2] == board[2][0]){
            return true;
        }
        return false;
    }
    private boolean draw(){
        for( int i = 0; i < 3; i++){
            for( int j = 0; j < 3; j++){
                if(board[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
    private void play(){
        Scanner sc = new Scanner(System.in);
        newBoard();
        System.out.println("|--------------------|");
        System.out.println("| Welcome To OX Game |");
        System.out.println("|--------------------|");
        System.out.print("Player 1, choose 'X' or 'O': ");
        char p1 = sc.nextLine().toUpperCase().charAt(0);
        
        while(p1 != 'X' && p1 != 'O'){
            System.out.println("Invalid choice. Please chose 'X' or 'O': ");
            p1 = sc.nextLine().toUpperCase().charAt(0);
        }
        
        char p2 = (p1 == 'X')? 'O':'X';
        System.out.println("Player 1: "+ p1);
        System.out.println("Player 2: "+ p2);
        char currentPlayer = p1;
        boolean gameEnded = false;
        createBoard();
        while(!gameEnded){
            System.out.print("It's player "+currentPlayer+" turn. Please input row[1-3] and column[1-3] for your move : ");
            int row = sc.nextInt()-1;
            int col = sc.nextInt()-1;
            
            if(row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-'){
                board[row][col] = currentPlayer;
                createBoard();
                if(hasWon()){
                    System.out.println("Congratulation! Player "+currentPlayer+" wins!");
                    gameEnded = true;
                }
                else if(draw()){
                    System.out.println("Draw!");
                    gameEnded = true;
                }
                else{
                    currentPlayer = (currentPlayer == 'X')? 'O':'X';
                }
        }
            else{
               System.out.println("Invalid move Please try again.");
            }
        }
        System.out.println("Do you want to play again? (y/n) : ");
        String yn = sc.next();
        if(yn.equals("y")){
            play();
        }else{
            System.out.print("Bye!");
        }
    }

    public static void main(String[] args) {
        Lab01 game = new Lab01();
        game.play();
    }
}
